import java.util.Hashtable;

public class TwoSum {
    public static int[] twoSum(int[] nums, int target) {
        // Il faut minimum deux éléments pour l'addition.
        if (nums.length >= 2) {
            Hashtable values = new Hashtable();

            for (int index = 0; index < nums.length; index++) {
                /*
                    On recherche la deuxième valeur qu'il nous faut pour atteindre
                    target à chaque étape de la boucle.
                 */
                int complement = target - nums[index];

                // Si le numéro est présent, nous avons nos deux numéros.
                if (values.containsKey(complement)) {
                    int indexFirstNumber = (int) values.get(complement);
                    return new int[]{ indexFirstNumber, index };
                }

                // Sinon on ajoute le numéro et son index dans la HashTable
                values.put(nums[index], index);
            }
        }

        return null;
    }
}
