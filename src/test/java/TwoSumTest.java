import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class TwoSumTest {
    @Test
    public void twoSum_withArrayLengthInferiorAtTwo_equalsEmptyArray() {
        int[] nums = { 1 };
        int target = 1;

        assertTrue(TwoSum.twoSum(nums, target) == null);
    }
    @Test
    public void twoSum_withArrayContainsTwoSevenElevenAndFifteen_andTargetNine_equalArrayWithZeroAndOne() {
        int[] nums = { 2, 7, 11, 15 };
        int target = 9;

        int[] resultExpected = { 0, 1 };
        int[] result = TwoSum.twoSum(nums, target);

        assertTrue(result.length == 2);
        assertTrue(Arrays.equals(result, resultExpected));
    }
    @Test
    public void twoSum_withArrayContainsThreeTwoFour_andTargetSix_equalArrayWithOneAndTwo() {
        int[] nums = { 3, 2, 4 };
        int target = 6;

        int[] resultExpected = { 1, 2 };
        int[] result = TwoSum.twoSum(nums, target);

        assertTrue(result.length == 2);
        assertTrue(Arrays.equals(result, resultExpected));
    }
    @Test
    public void twoSum_withArrayContainsThreeAndThree_andTargetSix_equalArrayWithZeroAndOne() {
        int[] nums = { 3, 3 };
        int target = 6;

        int[] resultExpected = { 0, 1 };
        int[] result = TwoSum.twoSum(nums, target);

        assertTrue(result.length == 2);
        assertTrue(Arrays.equals(result, resultExpected));
    }
    @Test
    public void twoSum_withArrayContainsZeroFiveAndSix_andTargetSix_equalArrayWithZeroAndTwo() {
        int[] nums = { 0, 5, 6 };
        int target = 6;

        int[] resultExpected = { 0, 2 };
        int[] result = TwoSum.twoSum(nums, target);

        assertTrue(result.length == 2);
        assertTrue(Arrays.equals(result, resultExpected));
    }
    @Test
    public void twoSum_withArrayContainsNegativeNumbers_andTargetNegativeHeight_equalArrayWithNegativeThreeAndFive() {
        int[] nums = { -1, -2, -3, -4, -5 };
        int target = -8;

        int[] resultExpected = { 2, 4 };
        int[] result = TwoSum.twoSum(nums, target);

        assertTrue(result.length == 2);
        assertTrue(Arrays.equals(result, resultExpected));
    }
    @Test
    public void twoSum_withArrayContainsThreeAndNegativeThree_andTargetZero_equalArrayWithThreeAndNegativeThree() {
        int[] nums = { -3, 4, 3, 90 };
        int target = 0;

        int[] resultExpected = { 0, 2 };
        int[] result = TwoSum.twoSum(nums, target);

        assertTrue(result.length == 2);
        assertTrue(Arrays.equals(result, resultExpected));
    }
}
