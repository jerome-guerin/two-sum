# Two Sum

## But du projet


❓ Nous avons **nums** qui est un tableau d'int, **target** un int et le but est de trouver deux nombres parmi ceux de nums qui ensemble valent target et retourner **un tableau avec leur index**.

Exemple :
- int[] nums = { 1, 2, 3, 4 }
- int target = 5

Le résultat attendu est un tableau contenant 1 et 2 puisque ce sont les index des valeurs 2 et 3 qui ensemble font 5 (target).

<ins>Résultat :</ins> [1, 2]

### Lancer le projet

#### Pré-requis

- Java 8
- Maven 3.8.1
- Junit 4.6

```shell
mvn clean install
mvn test
```

#### Changer l'algorithme

Tout se situe dans le fichier présent dans l'arborescence suivante :

> two-sum\src\main\java\TwoSum.java